import { Router, Request, Response } from 'express';
import { dataValidation } from '../middlewares/dataValidation';

// Creamos nuestro router
const testRoutes = Router();

// Esuchamos peticiones GET en /test/error para pruebas del error handler
testRoutes.get('/error', (req: Request, res: Response) => {
    throw new Error('Esto es un error interno del servidor y es manejado con una función en el index.ts');
});

// Creamos el endpoint y realizamos la suma, resta, multiplicación y división de el arr enviado
// Adicionalmente se pasa por el middleware de validación de datos para asegurar que el body obtenido
// sea exclusivamente un array que contenga números.
testRoutes.post('/', [dataValidation], (req: Request, res: Response) => {

    const arr = req.body;

    res.status(200).json({
        data: {
            suma: sum(arr),
            resta: subtract(arr),
            multiplicacion: multiply(arr),
            division: divide(arr)
        },
        errors: []
    });

});

// Declaración de funciones de procesamiento de DATA
function sum(array: Array<number>) {
    return array.reduce((sum, value) => sum + value);
}

function subtract(array: Array<number>) {
    return array.reduce((sum, value) => sum - value);
}

function multiply(array: Array<number>) {
    return array.reduce((sum, value) => sum * value);
}

function divide(array: Array<number>) {
    return array.reduce((sum, value) => sum / value);
}

export default testRoutes;