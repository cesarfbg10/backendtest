import testRoutes from './test.route';
import Server from '../classes/server';
import request from 'supertest';

describe('Pruebas a la clase Server', () => {

    let httpServer: any;
    let server: any;

    beforeAll(() => {
        server = new Server();
        server.app.use('/test', testRoutes);
        httpServer = server.start(() => {});
    });

    afterAll(() => {
        httpServer.close();
    });

    it('Debe exisitir una ruta exportada', () => {
        expect(testRoutes).toBeDefined();
    });

    describe('Pruebas al endpoint /test/error', () => {
        
        it('Debe retornar un status 500', async () => {
            const res = await request(server.app).get('/test/error');
            expect(res.status).toEqual(500);
        })

    });

    describe('Pruebas al endpoint /test ante errores', () => {

        it('Debe retornar un status 422 cuando NO se envía un array válido', async () => {
            const res = await request(server.app).post('/test');
            expect(res.status).toEqual(422);
        })

        it('Debe retornar una propiedad data en el body cuando NO se envía un array válido', async () => {
            const res = await request(server.app).post('/test').send(['prueba']);
            expect(res.body).toHaveProperty('data');
        })
        
        it('Debe retornar una propiedad errors en el body cuando NO se envía un array válido', async () => {
            const res = await request(server.app).post('/test').send([1, 2, 3, 4, 'string']);
            expect(res.body).toHaveProperty('errors');
        });

        it('Debe retornar una propiedad errors que contenga invalid_data_format cuando NO se envía un array válido', async () => {
            const res = await request(server.app).post('/test').send([1, 2, 3, 4, '5']);
            expect(res.body.errors).toContain('invalid_data_format');
        });
        
    });

    describe('Pruebas al endpoint /test sin errores', () => {

        it('Debe retornar un status 200 cuando SI se envía un array válido', async () => {
            const res = await request(server.app).post('/test').send([1, 2, 3, 4]);
            expect(res.status).toEqual(200);
        })

        it('Debe retornar una propiedad data cuando SI se envía un array válido', async () => {
            const res = await request(server.app).post('/test').send([1, 2, 3, 4]);
            expect(res.body).toHaveProperty('data');
        });

        it('La propiedad data debe contener una propiedad suma cuando SI se envía un array válido', async () => {
            const res = await request(server.app).post('/test').send([1, 2, 3, 4]);
            expect(res.body.data).toHaveProperty('suma');
        });

        it('La propiedad data debe contener una propiedad resta cuando SI se envía un array válido', async () => {
            const res = await request(server.app).post('/test').send([1, 2, 3, 4]);
            expect(res.body.data).toHaveProperty('resta');
        });

        it('La propiedad data debe contener una propiedad multiplicacion cuando SI se envía un array válido', async () => {
            const res = await request(server.app).post('/test').send([1, 2, 3, 4]);
            expect(res.body.data).toHaveProperty('multiplicacion');
        });

        it('La propiedad data debe contener una propiedad division cuando SI se envía un array válido', async () => {
            const res = await request(server.app).post('/test').send([1, 2, 3, 4]);
            expect(res.body.data).toHaveProperty('division');
        });

        it('La propiedad errors debe retornar un array vacío cuando SI se envía un array válido', async () => {
            const res = await request(server.app).post('/test').send([1, 2, 3, 4]);
            expect(res.body.errors.length).toBe(0);
        });
        
    });

});