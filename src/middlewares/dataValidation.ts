import { Request, Response, NextFunction } from 'express';

// Exportamos la función de validación del arr recibido en nuestro endpoint
export const dataValidation = (req: Request, res: Response, next: NextFunction) => {

    // Guardamos lo recibido en el body en una variable para ser validada
    const numberArray = req.body;

    // Creamos una bandera para saber al final si podemos llamar el método next o no
    let passFilter: boolean = true;

    // Inicialmente validamos si lo que recibimos es un arreglo, sino, enviamos respuesta de formato inválido
    if (Array.isArray(numberArray)) {

        // Si es un array procedemos a revisar cada elemento del array con el typeof, ya que la función NaN
        // Reconocería un elemento del array como "2" como un número, cuando realmente es un string
        for (let arrElement of numberArray) {
            
            // Si el tipo del elemento es diferente de number respondemos con 422 y la respuesta de formato inválido
            // Y terminamos el ciclo para no evaluar innecesariamente otros elementos
            if (typeof arrElement !== 'number') {
                passFilter = false;
                res.status(422).json({
                    data: '',
                    errors: ['invalid_data_format']
                });
                break;
            }
        }

        // Si ningun elemento cambió la bandera passFilter podemos llamar a la función next y seguir con la función
        if ( passFilter ) {
            next();
        }

    } else {

        res.status(422).json({
            data: '',
            errors: ['invalid_data_format']
        });
        
    }
}