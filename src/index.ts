import Server from "./classes/server";
import testRoutes from "./routes/test.route";

// Creamos una instancia de nuestra clase server
const server = new Server();

// Creamos el endpoint /test
server.app.use('/test', testRoutes);

// Iniciamos la escucha de nuestro server
server.start(() => {
    console.log(`Server running on port ${server.port}`);
});

// Manejador de errores y excepciones del servidor
// Se puede probar con el endpoint /test/error con una petición GET (plus adicional para pruebas)
server.app.use(function (err: any, req: any, res: any, next: any) {
    res.status(500).json({
        data: '',
        errors: ['internal_server_error']
    });
});