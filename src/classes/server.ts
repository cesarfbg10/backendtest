import express from 'express';
import bodyParser from 'body-parser';

export default class Server {
    
    // Inicializamos la aplicación de express y el puerto
    public app: express.Application;
    public port = process.env.PORT || 3000;

    // Al inicializarse hacemos uso del bodyParser para tratar la data recibida
    constructor() {
        this.app = express();
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(bodyParser.json());
    }

    // Declaramos un método para poner a andar nuestro server
    start( callback: Function ) {
        return this.app.listen(this.port, callback());
    }

}