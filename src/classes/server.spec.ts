import Server from './server';
import request from 'supertest';

describe('Pruebas a la clase Server', () => {

    const server = new Server();
    let httpServer: any;

    afterAll(() => {
        httpServer.close();
    });

    it('Las propiedades app y port deben estar definidas', () => {
        expect(server.app).toBeDefined();
        expect(server.port).toBeDefined();
    });

    it('Debe tener un método start para iniciar el servidor', () => {
        expect(server.start).toBeDefined();
    });

    it('El método start debe iniciar el server', async (done) => {
        httpServer = server.start(() => {});
        expect(httpServer).toBeDefined();
        done();
    });

    it('El servidor debe responder a cualquier petición', async(done) => {
        const result = await request(server.app).get('/');
        expect(result.status).toBe(404);
        done();
    });

});