"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = __importDefault(require("./classes/server"));
const test_route_1 = __importDefault(require("./routes/test.route"));
// Creamos una instancia de nuestra clase server
const server = new server_1.default();
// Creamos el endpoint /test
server.app.use('/test', test_route_1.default);
// Iniciamos la escucha de nuestro server
server.start(() => {
    console.log(`Server running on port ${server.port}`);
});
// Manejador de errores y excepciones del servidor
// Se puede probar con el endpoint /test/error con una petición GET (plus adicional para pruebas)
server.app.use(function (err, req, res, next) {
    res.status(500).json({
        data: '',
        errors: ['internal_server_error']
    });
});
