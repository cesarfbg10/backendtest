"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
class Server {
    // Al inicializarse hacemos uso del bodyParser para tratar la data recibida
    constructor() {
        this.port = process.env.PORT || 3000;
        this.app = express_1.default();
        this.app.use(body_parser_1.default.urlencoded({ extended: true }));
        this.app.use(body_parser_1.default.json());
    }
    // Declaramos un método para poner a andar nuestro server
    start(callback) {
        this.app.listen(this.port, callback());
    }
}
exports.default = Server;
