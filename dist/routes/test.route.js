"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const dataValidation_1 = require("../middlewares/dataValidation");
// Creamos nuestro router
const testRoutes = express_1.Router();
// Esuchamos peticiones GET en /test/error para pruebas del error handler
testRoutes.get('/error', (req, res) => {
    throw new Error('Esto es un error interno del servidor y es manejado con una función en el index.ts');
});
// Creamos el endpoint y realizamos la suma, resta, multiplicación y división de el arr enviado
// Adicionalmente se pasa por el middleware de validación de datos para asegurar que el body obtenido
// sea exclusivamente un array que contenga números.
testRoutes.post('/', [dataValidation_1.dataValidation], (req, res) => {
    const arr = req.body;
    res.status(200).json({
        data: {
            suma: sum(arr),
            resta: subtract(arr),
            multiplicacion: multiply(arr),
            division: divide(arr)
        },
        errors: []
    });
});
// Declaración de funciones de procesamiento de DATA
function sum(array) {
    return array.reduce((sum, value) => sum + value);
}
function subtract(array) {
    return array.reduce((sum, value) => sum - value);
}
function multiply(array) {
    return array.reduce((sum, value) => sum * value);
}
function divide(array) {
    return array.reduce((sum, value) => sum / value);
}
exports.default = testRoutes;
