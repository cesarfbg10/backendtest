# Servidor de pruebas en NodeJS para DXC Technology

## Detalles:

1. Implementación de express.
2. Uso del bodyParser.
3. Uso de routers.
4. Creación de peticion post en endpoint /test.
5. Creación de petición get en endpoint /test/error.
6. Uso de middlewares para validación de data recibida.
7. Manejo universal de errores y excepciones.

## Ejecutar el sever:

##### Abrir una terminal y posicionarse en la ruta donde se desee clonar el proyecto y ejecutar el comando:
`git clone https://github.com/cesarfbg/backendTest.git`

##### Luego navegar a la carpeta del proyecto con el comando: 
`cd backendTest`

##### Luego instalar las dependencias (node_modules) con el comando:
`npm i`

##### Finalmente poner a andar el server con el comando: 
`npm start`

## Documentación para consumir la API
[Documentación de las peticiones de la API REST](https://documenter.getpostman.com/view/6528581/SVtR3AbH?version=latest)

## Pruebas unitarias:

##### Para ejecutar todas las pruebas unitarias:
`npm test`

##### Para ejecutar todas las pruebas unitarias y ver la cobertura de las mismas:
`npm run coverage`

##### Para ver el detalle de la cobertura de las pruebas en el navegador: 
`cd coverage/lcov-report` 
##### y despues ejecutar: 
`index.html`

###### Desarrollador: César Blanco.
